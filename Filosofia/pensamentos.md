# Viver. #
**Alguns pensamentos**
## Se busca. ##
**algumas respostas que não posso dizer se estão corretas, ou mesmo inadequadas**.

**Entender**:
* esperança, recomeço.
A **esperança** nos dá condições de compreender os momentos onde movidos por emoções e muitas vezes instintos ainda de uma centelha animal em muitos casos importante para nossa forma de viver nos deixa um pocuo cegos por fúria, e outras emoções similares.
Muitas vezes o cansaço or situações que se repetem, não nos permitem ver os fatos por ângulos distintos e mais caridosos.
A esperança verdadeira deve ter a força de uma paixão, assim ela realmente provoca mudanças em nossas ações e reações alimentando o crescimento o desapego a ilusões do mundo e promovendo o amor
A cada passo, recomeçamos e sendo esses passos movidos pelas esperanças que trazemos começamos a tornar a esperança um novo modo de
enxergar nosso caminho.
O recomeço é necessário a cada passo por essa estrada que conhecemos e que particularmente denomido "vida".
* perdão, gratidão.
 
 **perdão** é um gesto de amor, porém é preciso coragem para se amar, mesmo que de uma forma direcionada a si mesmo, o perdão nos causa uma _paz_ muito importante para sermos capazes de agradecer a nós mesmos pelo direitor de nos amar.
 
 A **gratidão**, esta por sua vez se torna pessoal onde agradecemos a nós mesmos pela coragem de amar, amar sim a _nós_ mesmos e ao nosso próximo, Desta forma agradecemos e retribuímos a nós e a cada um de nosso atos, o maior desafio é ver no perdão a real oportunidade de amar.
 
 Para se perdoar, temos de ser capazes de perdoar a nós mesmos até mesmo por gesto que muitas vezes acabaram sendo reações que sem intenções destrutivas se tornam interpretadas de forma diferente do que realemnte tínhamos como intuito.
 
* amor, carinho.
O _amor_ pode estar simplesmente cheio e completo ao se observar em silêncio, mesmo que por um instante a beleza da liberdade, seja em nós mesmos ou até mesmo em uma vida que silenciosa nos dá a oportunidade de a ver com respeito. Ao se obesvar uma flor como exemplo mostramos a ela sua beleza refletida em nossos corações e olhares puros como uma declaração de amor a beleza singular e temporária de cada uma das flores que lembramos mesmo que de forma inconsciente a cada nova flor que observamos.
Ensinamentos budistas revelam esse gesto comparando-se ao gostar. Se ama uma flor devemos apenas regá-la; se gostamos de uma flor podemos ser egoístas e querermos a por em um vazo e olhar na ilusão de sermos seu dono.

* paz, equilíbrio.
* objetivos, alcançar.

## Se caminha. ##
**Compreender**:
A compreensão é um momento de comunhão onde realmente sentímos o fato da mesma forma porém jamais com a mesma intensidade.
Podemos compreender muitas coisas porém isso requer maturidade, conhecimento, esclarecimento e entendimento.
Esses por sua vez partem de nós mesmos.
Quando se tem entendimento sobre uma de nossas emoções, podemos ser capazes de compreender tal emoção e sentimento pelo amigos e próximos a quem temos carinho e enfrentam situações similares.
A compreensão pode ser entendida como sendo uma comparação inconsciente sobre um fato.
A partir dela somos capazes de com reflexão expressar nossos sentimentos como caridade, piedade, amizade, carinho e até mesmo amor.

ter:
    * Esperança.
    * Descobrir, _recomeço_.
    & Força, _perdoar_.
    * Reconhecimento, _agradecer_.
    * Dividir, _amar_.
    * Paz, _sentir_.
    
**Crescer**
    * Caminhar, entender, compreender.

# O caminho, alcançar. #

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
