# Filosofia #

**Compreendendo**.

- As primeiras causas.

    * O questionamento.

- Os últimos fins.

    * Resultados, compreensão, entendimento.


- Ideologia.

- Equilíbrio.

    * Satisfação.

    * Sucesso versus fama.

---

# Verdades #

Fatos, ciência.

>Verdades entre ciência e consciência.
A ciência e a evolução de suas áreas de exatas e humanas, permite ver na história contradições benéficas ao crescimento do consiênte coletivo.
A sociedade constrói seus moldes de padrões e costumes filosóficos, a partir de seu conhecimento dito cientifico.
Isso fazo como que a cultura permita o crescimento do entendimento do ser en si.

**A visão de si e o universo que o cerca**:

O ser humano desde seu nascimento possui influências do meio onde se encontra, isso mostra a ele através de seus sentidos, formas de explorar e assimilar informações sobre o que o cerca.

Conforme ele desenvolve sua percepção e seu conhecimento consciência e ciência de si, ele se permite a compreensão e o entendimento de su semelhante.

A relação filosófica dando interpretação a fatores espirituais e astrais, o que a meu ver são independentes porém relacionados diretamente com a evolução de cada um, formando um coletivo consciênte.

Me apresento de forma a não ser radical em nenhum de meus pensamentos, permitindo novas visões e abrindo as portas de minha emnte a fatos que a ciência e cultura humana vem a comprovar ao longo do tempo.


>A evolução.

>A expansão da consciência.


_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
