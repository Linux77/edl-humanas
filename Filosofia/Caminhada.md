## Um pouco sobre filosofia ##
**A corrida e a caminhada**.
* A corrida:
> A algum tempo participei de uma corrida.
> Essa foi justa e equilibrada, todos os competidores tinham o mesmo potencial.
> O objetivo era a vida.
> Venci, por isso acredito na caminhada que se iniciou logo em seguida.

* A caminhada:
> Na caminhada sempre tive companheiros, muito importantes.
> Estes me ensinam e ajudamos uns aos outros.
> Vejo ao longo da estrada, sempre juntos a mim; o perdão, o amor, a paz e a gratidão.

_Leonardo de Araújo Lima_.
