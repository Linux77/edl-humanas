## Redes sociais ivres ##

---
Aqui temos uma breve lista de recursos em nível de redes sociais livres, respeitando as liberdades essenciais propostas pela filosofia do software livre, idealizada para dar controle sobre as informações do usuário de tecnologia em telecomunicações e afins.

* [**Mastodon:**](http://www.joinmastodon.org)

* [**Friendica:**](http://www.friendi.ca)

* [**Pixelfed:**](http://www.sovacy.com)

* [**Funkwhale:**](http://www.funkwhale.audio)

* [**Peertube:**](http://www.joinpeertube.org)

Essa é uma tentativa de resgatar os ideais e a filosofia de liberdade proposta pela Free Software Foundation.
 
Tornando mais democrático o conhecimento de tecnologias e uso de alternativas livres ao público em geral.

Também é um dos meus propósitos e ideais tentar resgatar a idéia de um centro de estudos de tecnologia livre.

Por isso tento através de meus projetos e trabalho nos tempos recentes, formalizar a Academia do software livre, no caso com sede em São Lourenço no sul de Minas Gerais, BRasil.

---

_Leonardo de Araújo Lima_.

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
