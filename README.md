# Aqui temos uma série de estudos direcionados a ciências humanas #
    A proposta desse trabalho é difundir estudos e pesquisas que desenvolvo como professor e estudante de várias áreas das ciências humanas.
    
[*Índice dos estudos e propostas de pesquisa*](Estudos-dirigidos.md).
    
> Aqui temos uma lista de opções a nível de redes sociais, para partilharem cultura e vários recursos.
> Atuando dentro da proposta e da visão de comunicação e uso da tecnologia, respeitando as liberdades essenciais.
> No site da fundação, temos vários tópicos importantes abordados, como educação, filosofia, etc.
---

## **Se educar para não se escravizar** ##

O mundo da tecnologia moderna tem o aplicações que auxiliam praticamente todas as atividades humanas.

Nos vemos  diante de uma cultura global, onde aquele que não conhece tecnologia acaba se tornando vítima de seu desconhecimento. Muitas vezes, deixando isso a cargo de pessoas as quais, acredita terem conhecimento suficiente e senso crítico e moral para respeitar sua liberdade.

Muitas vezes não é assim que se dá na prática. Muitas tecnologia são usadas para limitar a própria forma de se relacionar em uma sociedade. Nesta sociedade, muitos se beneficiam da ignorância com relação a tecnologia, se aproveitando e direcionando massas populacionais, de acordo como queiram.

**Se faz necessário se educar para não se escravizar**.

**Horizontes**.

>Pode se dizer que de uma certa forma a ignorância, tende a limitar os horizontes e a forma de se relacionar consigo e com o universo de informações a seu redor.
**Solidão**.

>Por outro lado a ciência dos fatos e de si mesmo traz benefícios para sua forma de pensar.
>Ainda com relação ao estabelecimento de uma consciência crítica o conhecimento ao libertar, aumentar a sensibilidade e auxiliar no entendimento de seu semelhante, também traz uma sensação de solidão.

**A tecnologia vista como software**.

Hoje temos três grandes mercados e formas de entendimento no uso de tecnologia, são eles:

* **O software livre**.

* **O software open source**.

* **O software privativo**.

---

**Para saber mais sobre os direitos e a liberdade dos usuários e desenvolvedores de tecnologia, visite:**

* [Fundação do software livre](http://www.fsf.org)
* [Fundação *GNU*](http://gnu.org)

A participação dos usuários de tecnologia de forma livre e consciente, permite a evolução e troca entre diferentes culturas, filosofias, permitindo uma interação e um avanço a todas, através de um sincretismo cultural.


[Lista de redes sociais livres](redes_sociais_livres.md).


*Bem vindo a grato pela participação*.

Espero que gostem e aproveitem nossas propostas.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L *São Lourenço - MG : Brasil* Website](http://www.asl-sl.com.br).
