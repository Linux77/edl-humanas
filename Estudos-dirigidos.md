## **propostas de estudos dirigidos.**
*Leonardo de Araújo Lima*
# Psicologia.
## Dualidade do ser.
* Inconsciente:
  * Instintos(animal).
  
  Instintos naturais.
  Muitos destes comuns a maior parte de animais.
  
  No caso do ser humano, odemos comparar a mamíferos em geral.
  
  * Masculino, feminino.
  * *crise existencial*.
* Subconsciente.
  * Início racional.
  * O sono.

## Fisiologia:
  * O córtex cerebral.
  * Consciente.
    * Tratamento de informações.
    * Coeficiente de inteligencia emocional.
    * Processamento de linguagem.

## Didática
  * Construtivismo:
    * Conhecimento prévio.
      * Analogia.
      * construção de novos entendimentos.
      * Entendimento, compreensão.

## Didática convencional:
  * Estímulo de emoções prévias.
    
  * Método de construção da proficiência.

    * Ciência dos fatos.
      * Consciência, entendimento dos fatos.
      * compreensão dos fatos.
        * Mudança comportamental.
        * Proficiência dos fatos.
            * Prática.
            * Execução de atos.
            ( utilização do saber ).

## Astronomia ( geografia ).
* Movimento de translação, órbita.
* Movimento de Rotação, dia x noite.
* Posição geostática:
* Luz, sombra ao redor do Sol.
  * Retorno a posição.
  * Tempo de órbitas solar, terrestre:
  ***36 mil anos***

## Filosofia ( astrologia, ordem astral ).
  * Taoiísmo.
  * Xintuísmo.
  * Hinduísmo.
  * Budismo.
# O corpo, o universo.
  * Constelações ( galáxias ).
  * Nakshatras.
  * Sistema e orientação de castas.
   * Organização social.
    
## Organização social. 
* Nomastérios( matriarcado ).
 * Legitimação do poder.
* Atribuição religiosa(ordem espiritual).
* Patriarcado.
# O ser.            
* Compreensão de si.
  * Função e satisfação(realização) social.
  * *crise existencial*.
* Autoconhecimento.
  * O legado (manutenção da existência).
* Imortalidade:
  * Memória, manutenção da existência.
  * *crise existencial*.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
