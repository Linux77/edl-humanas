# Minha visão sobre a didática #
**contexto**.

Antes de mais nada, gostaria de me apresentar ajudando assim o entendimento sobre meus pensamentos.
Dese menino percebi que muitas coisas que para mim, se tornavam fáceis e em alguns casos, óbivias, para muito que me cercavam principalmente em sala de aula, eram menos claras, desde pequeno isso me motivou a aprender como aprender, e usar isso como uma espécie de talento para o aprendizado.
No ensino médio me graduei como técnico em magistério, na condição de professor para o ensino fundamental. Com meus quatorze anos já estava apto e dei início a minhas atividades de professor.

Posteriormente me graduei a nível superior nas áreas de humanas como estudos sociais e história.

Com minha ainda pequena bagagem como professor e alguns estudos em filosofia, psicologia e didática em sua forma prática.

Analisando pensamentos e aplicações práticas de métodos baseados em Piaget, a início, juntamente como Rousseau, observei que na realidade eu era apaixonado por aprender e adequar meus aprendizados, as outras pessoas e alunos.

***Aplicação**.
Vejo na didática a arte de aprender e dividir em seu sentido amplo , não só os fatos mas também a consciencia sobre eles e por fim sua proficiência, a qual implica na mudança de comportamento e (ou) prática de uma atividade.

**Técnica**.

Minha visão prática do **processo** _ensino aprendizagem_, se dá em apenas três etapas:

* A ciência.
>Nessa fase inicial a exposição das informações, deve ser o mais simples possível.

* A cosciência.
>Nessa fase, é observado e mostrado ao educando, tudo que se relaciona com o fato.
>Todas as aplicações e funcionalidade, bem como métodos para a construção do entendimento.

* A proficiência.
>Nesta etapa, a anterior a fase de consciência se torna prática e efetiva o conhecimento em sua aplicação na mudança comportamental,e ou prática de uma atividade.

Seguindo essas três bases como etapas para o processo de aprendizagem, ao longo de meus mais de vinte anos como educador em várias formas e disciplinas distintas em cursos de especialização profissional para jovens e adultos, percebi uma grande receptividade e também um grande sucesso na tarefa de preparar não só em questões tecnicas mas também em tornar o educando capaz de interagir de forma direta como aprendizado, dando vida a seue pensamentos dentro do conteúdo a ser ministrado.

---
## Aspectos práticos ##

Dentro dos conteúdos que muitos profissionais da área de educação realizam, temos
algumas divergências, o que é bom, pois impulsiona o avanço.
Seguindo o que nos é mostrado em psicologia da educação, um contato direto com o trabalho
de Jean Piaget, nos mostra como associar informações a emoções e instintos.

Pelo lado da própria fisiologia do ser humano, isso é uma técnica com excelentes
resultados práticos. Por outro lado temos no construtivismo, concebido por estudos de Paulo Freire, uma abordagem do ensino a partir de pequenas peças chave para o aprendizado.

Juntando ambos os métodos, podemos aprimorar o processo de ensino aprenndizagem, levando o estudo a três momentos.

- **Primeiro estágio**.

>A ciência dos fatos.

- **Segundo estágio**.

>A consciência dos fatos.

- **Terceiro estágio**.

>A prática dos atos.
>A proficiência, ou seja mudança comportamental, e ou prática de uma atividade.
>Está é o objetivo da educação, o último fim, como nos mostra a filosofia.

>Dentro de aspectos práticos, psicológicos e filosóficos. Essas três etapas, podem ser trabalhadas no planejamento do  ensino.

## Apresentação dos fatos ##

Nesse momento inicial aonde o educando precisa de uma motivação adequada para a ficação do conteúdo.
>Pode-se ainda dizer que ao educador será dada a responsabilidade ao mostrar, com diferentes pontos de vista, a existência do fato:
>Posso citar como um exemplo, a exposição e atribuição de imagens e objetos a conceituação dos mesmos.
>Partindo dessa visão o educador pode trabalhar conteúdos de matemática por exemplo apresentando os objetos ou imagens, como elementos de um conjunto.
 >Em uma segunda etapa, a consciência sobre a exposição a esses fatos(objetos, imagens), dará condições ao educador a associar a definição de conjuntos.

## A consciência dos fatos ##


_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
