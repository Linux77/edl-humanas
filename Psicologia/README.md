# O entendimento. #

**O entendimento e a compreenssão**.

* **Entendimento**.

Em relacionamentos sociais, A partir de um ponto de vista prático:

Ao meu ver, com alguma experiência na observação do comportamento de muitos; vejo que a mior parte das pessoas mesmo
de forma inconsciente, se projetam nas pessoas que mantém alguma espécie de contato com elas. Psso assim exemplificar como
se cada um visse inicialmente uma forma de espelho, onde imagina naqueles com que se realiona, muitas características próprias de si.

Assim, espera encontrar a mesma interpretação das idéias que expõe, como ele mesmo é capaz de interpretar.

Ao passar do tempo e do conhecimento mútuo entre ele e aquele com que tem contato, aos poucos irá permitindo se aprofundar. tanto no
conhecimento de si, o que se dá em ambos e permite o estreitamento dos laços e a aproximação entre as partes.

Pode-se observar alguns aspectos como essa projeção de si, permite tanto acertos como erros no convívio social.

Uma vez que o indivíduo se vê, ele é capaz de onsrvar características em comum. Uma vez encontradas tais características, caso sejam positivas, ocorre a aproximação, caso ele veja algunas coisas que o mesmo não aprecia em si, ele irá atribuir tais fatores apenas
aquele com quem se relaciona, como uma espécie de auto crítica, porém essa se dá de forma inconsciente e em alguns casos acaba por
afastar as pessoas.

* **Compreenssão**

Em seu caminhar, buscando a realização e satisfação:
* Metas.
Alcançar versus conquistar.

* Ideais, princípios.

Formação, experiências e vivência.
O ambiente social:
O ambiente que o ser tem desde sua primeira consciência de si, tem muita influência
em sua forma de aprender a aprender. Assim a visão do indivíduo e educando, adquire tendências e moldes.
Mesmo em contato com um ambiente favorável a uma construção de uma personalidade
adequada dentro dos modelos de ética e respeito, o mesmo tem atitudes diferentes das
que ele vivência desde sua infância.
Existem muitos exemplos desse fato tanto de forma positiva quanto negativa.
O ambiente social.
A cultura geral de um grupo social seja ele de tamanhos e proporções diferentes, permite o controle e direcionamento para fins, tanto positivos quanto negativos.
Em muitas culturas se vê, formas de convivência harmoniosa e pacífica.
O nível de conhecimento e respeito de uns pelos outros; tende a acompanhar a cultura geral.

## A Formação da mente, consciência ##

**O crescimento**.

* fases desde o nascimento

- O bebê:

Os instintos.

- Fase anal.

O prazer. A sensação de prazer ao iniciar o controledo esfincter.

Funções de controle, urina, fezes.

- Fase oral.

O prazer, sentir o todo a seu redor com o aprimoramento da visão.

Segundo momento, o início do contato com o mundo através do olfato e do paladar.

Experimentar procurar sabores em tudo que o cerca.

**O lúdico**.

A Forma inicial de interesse eminterpretar as informações e o mundo que o cerca.

O universo familiar.

A organização de visão sobre si mesmo.

A necessidade de se ver não como o tudo que está a seu redor mas sim como parte de um grupo.

A visão de sua família como o todo que pode ter contato.

A expressão.

A interpretação.

Hierarquia em convívio social.

**O cognitivo**.

O início do controle de suas emoções, a necessidade de interpretar as informações.

Os instintos, e a necessidade de associar as informações a nomes.

A necessidade de definições e conceitos para todos os componentes do universo que este tem contato.

A iniciativa de questionar.

A busca por respostas.

A forma de apresentar e expor suas necessidades.

A forma de entender e iniciar a deduzir.

O estímulo a dedução.

O raciocínio lógico.



**Crises existênciais**.

- **Sexual**.

- **Profissional**.

A vocação, (latin) "Vocare", chamado, chamamento. Instinto atrelado a dons e estímulos ou ainda facilidades cognitivas afetivas e de várias outras formas apresentadas a sensibilidade e intelecto.

- **O legado**.

Em uma determinada fase da maturidade o ser se questiona sobre o que deixará como memória para seus semelhantes.

Ao se aproximar de uma fase mais próxima do dito idoso, muito buscam em pequenas satisfações satisfazer sua real insatisfação com seu legado.

Em muitos casos apresenta-se na idade madura uma constante busca por realizações momentãneas, como forma de suprir a carência de um real legado.


### A realização ###

**Sucesso**:

Superação, relaizações individuais.

O sucesso não está relacionado de forma direta com a fama, uma vez que o sucesso pode ser subjetivo e trazer a sensação de realização mesmo em tarefas simples porém que necessitam de dedicação e muitas vezes implicam em um aprendizado.


A satisfação muito tem em comum com o prazer, sendo vista errõneamente como necessidade de subjulgar seus semelhantes ou ao menos se destacar.

**Fama**:

Reconhecimento pelo seu semelhante.

A necessidade de alguns em ter uma aprovação de seu semelhante ou do meio em que convive vem de uma carência afetivar promovida por insegurânca.

Em muitos casos isso se torna uma fuga para seus medos, onde ao ser reconhecido pelo grupo social ou seus semelhanantes se sente liberto, de uma constante insatisfação com si mesmo.




---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)


