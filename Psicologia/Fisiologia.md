# A fisiologia relacionada ao funcionamento da mente #

Uma das áreas da medicina muito importante pelos avanços da forma e modo de vida do homem moderno é a fisiologia.

Ela se aplica ao estudo do funcionamento orgâncio e na maior parte química do corpo dos seres vivos; em geral sendo aplicada diretamente no conhecimento do animal "Homem".

O cérebro, possui várias funções e a forma e modo de mantermos seu funcionamento mantém a saúde, ele influenciará em todo o funcionamento do organismo.

Os animais de sangue quente, mamíferos em geral.

## A energia ##

O quanto é consumido da energia produzida pelo organismo humano, pelo nosso orgão cérebro?
>Segundo estudos que realizei de forma livre e independente, constatei que o cérebro é o orgão que mais consome a energia produzida pelo organismo.

>*O quanto**?
>Cerca de 64% da energia produzida!

## O funcionamento ##

Como professor desde ensino fundamental a ensino médio, seguindo minhra formação acadêmica, estudei a psicologia por aproximados seis anos.

Com relação ao funcionamento do cérebro como instrumento do **pensar**, posso afirmar algumas coisas, muitas delas com visão prática tendo por objeto de estudo a mim mesmo:

**Necessidades**:

Na idade madura, depois de outras fases de sua evolução e entendimento de si, o que na maior parte dos casos implica na superação de crises existênciais comuns ao ser humano, a busca pelo legado a ser deixado, pode ser vista de várias formas. Aqui adotei o Direcionamento a um legado cultural.

O prazer de pensar, deduzir, concluir, racionalizar fatos e muitas informações que na maior parte das vezes tem importância direta no cotidiano e vida do ser humano, satisfaz, completa o lado emocional do ser humano.

Do próprio latin onde teve o início a construção de nosso idioma, a palavra saber, tem sua origem em sapere, que pode ser interpretado como saborear. Com essa visão o saber, aprender traz satisfação e em muitos casos sacia Necessidades emocionais.

Como o alimento sacia o corpo o saber tende a saciar a mente.


* Motivação:

A motivação é essencial para que o lado emocional do ser atue junto ao racional, dando força, forma e direcionamento ao exercício de pensar.

Existem muitas formas e métodos de motivação.

No exercício do magistério como em várias outras atividades temos a motivação como objetivo para a tomada de decisões.

A partir do primeiro passo se inicia uma caminhada.

Uma vez que o emocional tem influência direta na capacidade intelectual e física para o exercício de uma atividade, o nível de aproveitamento na atividade está relacionado a motivação e a condição emocional.

Um dos meios de se atingir a motivação, que costumo utilizar bastante é a observação de metas anteriores que foram alcançadas.

**QI**:

O coeficiênte de Inteligência?

Em estudos clássicos e de certa forma incompletos, o nível de velocidade de aprendizado e relacionamento de informações, o que demonstra a condição e capacidade de um indivíduo, era mensurado de forma estática. Dessa forma ele seria medido e uma vez cálculado, daria uma classificação ao ser.

Esse estudo apresenta o qoeficiente de inteligência.

Os estudos baseados nesse pensamento dizem que a capacidade, condição e estado da mente, tem um foco de facilidade para algumas áreas de estudo, o que em outras palavras seria, uma vez que o indivíduo possui aptidões para uma ciência exata, ele teria dificuldades em outras áreas.

**QIE**:

O coeficiênte de inteligência emocional.

_O cotéx cerebral_:

* Descanso:

> A mente através dew sua fisioloigia, ou seja seu funcionamento químico dentre outros fatores físicos, precisa de intervalos de descanso, esse pensamento torna possível a atuação de métodos para aumentar a capacidade de estudo e desenvolvumento.

> Além de pensamento esse é o resultado de uma série de estudos os quais dão base a várias ciências relacionadas com o estudo do ser humano.

> O córtex assume a função de receber as informações dos sentidos naturais do ser humano, decido ao número muito grande de informações ele precisa se refazer com uma velocidade alta, o que temos como base um tempo de aproximados 45 minutos.

* Atenção:

> A atenção ou foco, pode ser entendido e representado como período produtivo.

> Esse momento é onde os sentidos são direcionados a um evento, fato ou informação específica.

* Direcionamento:

* Produtividade:

* Criatividade:
